export interface Task {
  id?: string;
  message: string;
  due_date?: string;
  priority?: string;
  assigned_to?: string;
  assigned_name?: string;
  created_on?: string;
}
