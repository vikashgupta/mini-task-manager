import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Task } from './task.model';
import { TasksService } from './tasks.service';
import {
  style,
  state,
  animate,
  transition,
  trigger,
} from '@angular/animations';

const REMOVE_WARNING = 'Do you really want to remove this task?';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        // :enter is alias to 'void => *'
        animate(500, style({ transform: 'scale(1)' })),
      ]),
      transition(':leave', [
        // :leave is alias to '* => void'
        animate(500, style({ transform: 'scale(0)' })),
      ]),
    ]),
  ],
})
export class TasksComponent implements OnInit {
  tasks$ = this.taskService.taskList$;
  activeView$ = this.taskService.activeView$;
  isLoading$ = this.taskService.isLoading$;
  inCreateMode$ = this.taskService.inCreateMode$;
  inUpdateMode$ = this.taskService.inUpdateMode$;
  inDeleteMode$ = this.taskService.inDeleteMode$;
  viewBy$ = this.taskService.viewBy$;
  totalTasks$ = this.taskService.totalTasks$;

  priorities = this.taskService.priorities;
  selectedTask = null;
  selectedListKey = null;
  constructor(private taskService: TasksService) {}

  get viewBy(): string {
    return this.taskService.viewBy;
  }

  ngOnInit(): void {
    this.taskService.getUserList();
    this.taskService.getTaskList();
  }

  initCreateTaskMode(status: boolean, selectedListKey: string): void {
    this.selectedListKey = selectedListKey;
    this.taskService.setUpdateMode(!status);
    this.taskService.setCreateMode(status);
  }

  initUpdateTask(task: Task, status: boolean): void {
    this.taskService.setCreateMode(!status);
    this.taskService.setUpdateMode(status);
    this.selectedTask = task;
  }
  deleteTask(task: Task): void {
    const confirm = window.confirm(REMOVE_WARNING);
    if (confirm) {
      this.selectedTask = task;
      this.taskService.deleteTask(task.id);
    }
  }
  onItemDrop(data: any) {
    console.log('change currentPriority', data);
    if (this.viewBy === 'priority') {
      const { dragData: draggedTask } = data.event;
      const currentPriority = draggedTask.priority;
      if (currentPriority !== data.onDropped) {
        draggedTask.taskid = draggedTask.id;
        draggedTask.priority = data.onDropped;
        delete draggedTask.id;
        this.selectedListKey = data.onDropped;
        this.taskService.updateTask(draggedTask).subscribe(() => {
          this.taskService.getTaskList();
        });
      }
    }
  }
}
