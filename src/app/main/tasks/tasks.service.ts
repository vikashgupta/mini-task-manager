import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { finalize } from "rxjs/operators";
import { Task } from "./task.model";
import { environment } from "src/environments/environment";
import { UtilityService } from "./../../services/utility.service";

const { apiEndPoint } = environment;
const defaultTaskList: Task[] = [];
type UpdatedTask = Task & { taskid: string };

@Injectable({
  providedIn: "root",
})
export class TasksService {
  constructor(
    private httpClient: HttpClient,
    private utilityService: UtilityService
  ) {}

  private _isLoading = new BehaviorSubject(false);
  private _taskList = new BehaviorSubject(defaultTaskList);
  private _activeView = new BehaviorSubject({ List: defaultTaskList });
  private _userList = new BehaviorSubject([]);
  private _errorMessage = new BehaviorSubject(null);
  private _inCreateMode = new BehaviorSubject(false);
  private _inUpdateMode = new BehaviorSubject(false);
  private _inDeleteMode = new BehaviorSubject(false);
  private _viewBy = new BehaviorSubject("default");
  private _searchTask = new BehaviorSubject("");
  private _totalTasks = new BehaviorSubject(defaultTaskList.length);

  isLoading$ = this._isLoading.asObservable();
  errorMessage$ = this._errorMessage.asObservable();
  taskList$ = this._taskList.asObservable();
  activeView$ = this._activeView.asObservable();

  userList$ = this._userList.asObservable();
  inCreateMode$ = this._inCreateMode.asObservable();
  inUpdateMode$ = this._inUpdateMode.asObservable();
  inDeleteMode$ = this._inDeleteMode.asObservable();
  viewBy$ = this._viewBy.asObservable();
  searchTask$ = this._searchTask.asObservable();
  totalTasks$ = this._totalTasks.asObservable();
  priorities = {
    1: "Normal",
    2: "Medium",
    3: "High",
  };

  get viewBy(): string {
    return this._viewBy.getValue();
  }

  get searchTask(): string {
    return this._searchTask.getValue();
  }

  get taskList(): any[] {
    return this._taskList.getValue();
  }

  setCreateMode(status: boolean): void {
    this._inCreateMode.next(status);
  }

  setSearchText(text: string): void {
    this._searchTask.next(text.trim());
  }

  setUpdateMode(status: boolean): void {
    this._inUpdateMode.next(status);
  }

  setViewBy(view: string): void {
    this._viewBy.next(view);
    this.setActiveView();
  }

  setActiveView(): void {
    const data = this.groupBy(this.taskList, this.viewBy, this.searchTask);
    const totalTasks: any = Object.values(data).reduce(
      (acc: [], val: []) => acc.concat(val),
      []
    );
    this._totalTasks.next(totalTasks.length);
    this._activeView.next(data);
  }

  setLoading(status: boolean): void {
    this._isLoading.next(status);
  }

  setTaskList(taskList: Task[]): void {
    this._taskList.next(taskList);
  }

  setUserList(list: any[]): void {
    this._userList.next(list);
  }

  setErrorMessage(message: string): void {
    this._errorMessage.next(message);
  }

  getUserList(): void {
    const getUrl = `${apiEndPoint}/listusers`;
    this.httpClient.get(getUrl).subscribe((result: any) => {
      const { status } = result;
      if (status && status === "error") {
        this.setErrorMessage(result.error);
      } else {
        this.setUserList(result.users);
      }
    });
  }

  getTaskList(): void {
    const getUrl = `${apiEndPoint}/list`;
    this._isLoading.next(true);
    this.httpClient
      .get(getUrl)
      .pipe(
        finalize(() => {
          this._isLoading.next(false);
        })
      )
      .subscribe((result: any) => {
        const { status } = result;
        if (status && status === "error") {
          this.setErrorMessage(result.error);
          this._totalTasks.next(0);
        } else {
          this.setTaskList(result.tasks);
          this.setActiveView();
        }
      });
  }

  createTask(task: Task): Observable<any> {
    const createUrl = `${apiEndPoint}/create`;
    this._isLoading.next(true);
    const data = this.getFormData(task);
    return this.httpClient.post<Task>(createUrl, data).pipe(
      finalize(() => {
        this._isLoading.next(false);
      })
    );
  }

  updateTask(task: UpdatedTask): Observable<any> {
    const updateUrl = `${apiEndPoint}/update`;
    this._isLoading.next(true);
    const data = this.getFormData(task);
    return this.httpClient.post<UpdatedTask>(updateUrl, data);
  }

  deleteTask(taskid: string): void {
    const deleteUrl = `${apiEndPoint}/delete`;
    this._inDeleteMode.next(true);
    const data = this.getFormData({ taskid });
    interface DeleteParams {
      taskid: string;
    }
    this.httpClient
      .post<DeleteParams>(deleteUrl, data)
      .pipe(
        finalize(() => {
          this._inDeleteMode.next(false);
          // refresh the task list
          this.getTaskList();
        })
      )
      .subscribe((result: any) => {
        const { status } = result;
        if (status && status === "error") {
          this.setErrorMessage(status.error);
        }
      });
  }

  groupBy(data: Task[], key: string, searchTask: string): any {
    const groupedData = data.reduce((result, item) => {
      let dataKey = item[key];
      if (!dataKey) {
        dataKey = "List";
      }
      const message = item.message.toLowerCase();
      searchTask = searchTask.toLowerCase();
      if (message.includes(searchTask)) {
        if (result[dataKey]) {
          result[dataKey].push(item);
        } else {
          result[dataKey] = [item];
        }
      }
      return result;
    }, {});
    return groupedData;
  }

  getFormData(task: any): FormData {
    const formData = new FormData();
    for (const taskKey in task) {
      if (!this.utilityService.isEmpty(task[taskKey])) {
        formData.append(taskKey, task[taskKey]);
      }
    }
    return formData;
  }
}
