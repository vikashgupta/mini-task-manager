import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { FormGroup, FormControl, Validators } from "@angular/forms";
import { TasksService } from "./../tasks.service";
import { UtilityService } from "./../../../services/utility.service";
import { Observable } from "rxjs";

@Component({
  selector: "app-task-form",
  templateUrl: "./task-form.component.html",
  styleUrls: ["./task-form.component.css"],
})
export class TaskFormComponent implements OnInit {
  tasks$ = this.taskService.taskList$;
  userList$ = this.taskService.userList$;
  priorities = this.taskService.priorities;
  inCreateMode$ = this.taskService.inCreateMode$;
  isLoading$ = this.taskService.isLoading$;
  @Input("task") task: any = null;
  @Input("priority") priority = null;
  @Input("due_date") due_date = null;
  taskForm: FormGroup;
  buttonTitle = "Create";
  constructor(
    private taskService: TasksService,
    private utilityService: UtilityService
  ) {}

  ngOnInit(): void {
    this.initForm();
    if (this.task) {
      this.setForm();
      this.buttonTitle = "Update";
    }
  }

  initCreateTaskMode(status: boolean): void {
    if (this.task) {
      this.taskService.setUpdateMode(status);
    } else {
      this.taskService.setCreateMode(status);
    }
  }

  submitTask(): void {
    if (this.taskForm.valid) {
      const payload = {
        ...this.taskForm.value,
      };
      let operation: Observable<any> = this.taskService.createTask(payload);
      this.buttonTitle = "Creating";
      if (this.task) {
        payload.taskid = this.task.id;
        operation = this.taskService.updateTask(payload);
        this.buttonTitle = "Updating";
      }
      operation.subscribe((result: any) => {
        const { status } = result;
        if (status && status === "error") {
          this.taskService.setErrorMessage(status.error);
        } else {
          this.taskService.getTaskList();
          this.initCreateTaskMode(false);
        }
      });
    } else {
      const { message } = this.taskForm.value;
      if (this.utilityService.isEmpty(message)) {
        this.taskForm.markAllAsTouched();
      }
    }
  }

  initForm(): void {
    this.taskForm = new FormGroup({
      message: new FormControl("", Validators.required),
      priority: new FormControl(this.priority || ""),
      assigned_to: new FormControl(null),
      due_date: new FormControl(""),
    });
  }

  setForm(): void {
    const { message, priority, assigned_to, due_date, id } = this.task;
    console.log(priority);
    this.taskForm.setValue({
      message,
      priority,
      assigned_to: assigned_to ? assigned_to : null,
      due_date,
    });
  }
}
