import { Component, OnInit } from '@angular/core';
import { TasksService } from './../tasks/tasks.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
export class NavbarComponent implements OnInit {
  viewBy = [
    { text: 'Default', value: 'default' },
    { text: 'Priority', value: 'priority' },
    { text: 'Due Date', value: 'due_date' },
  ];
  selectedView: FormControl;
  searchText: FormControl;
  constructor(private taskService: TasksService) {}

  ngOnInit(): void {
    this.selectedView = new FormControl(this.viewBy[0].value);
    this.searchText = new FormControl('');
    this.searchText.valueChanges.subscribe((text) => {
      this.taskService.setSearchText(text);
      this.taskService.setActiveView();
    });
    this.selectedView.valueChanges.subscribe((text) => {
      this.taskService.setViewBy(text);
    });
  }
  clearSearch(): void {
    this.searchText.setValue('');
  }
}
