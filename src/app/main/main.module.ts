import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptor } from './../auth/token.interceptor';
import { NgDragDropModule } from 'ng-drag-drop';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { NavbarComponent } from './navbar/navbar.component';
import { TasksComponent } from './tasks/tasks.component';
import { TaskFormComponent } from './tasks/task-form/task-form.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoadingIconComponent } from '../components/loading-icon/loading-icon.component';

@NgModule({
  declarations: [
    MainComponent,
    NavbarComponent,
    TasksComponent,
    TaskFormComponent,
    LoadingIconComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NgDragDropModule.forRoot(),
    MainRoutingModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true,
    },
  ],
})
export class MainModule {}
