import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class UtilityService {
  constructor() {}

  isEmpty(text: string): boolean {
    let result = false;
    if (
      text === undefined ||
      text === null ||
      text === "null" ||
      !text.length ||
      !text.trim().length
    ) {
      result = true;
    }
    return result;
  }
}
