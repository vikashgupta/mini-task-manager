import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const { authToken } = environment;

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor() {}

  getToken(): string {
    return authToken;
  }
}
